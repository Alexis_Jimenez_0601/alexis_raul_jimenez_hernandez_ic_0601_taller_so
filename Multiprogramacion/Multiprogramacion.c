#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void* thread_run(void* data)
{ sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 1 \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 1 Terminado...............\n",pthread_self());
   sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 2  \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 2 Terminado...............\n",pthread_self());
  sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 3 \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 3 Terminado...............\n",pthread_self());
   sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 4  \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 4 Terminado...............\n",pthread_self());
  sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 5 \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 5 Terminado...............\n",pthread_self());
   sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 6  \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 6 Terminado...............\n",pthread_self());
  sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 7 \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 7 Terminado...............\n",pthread_self());
   sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 8  \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 8 Terminado...............\n",pthread_self());
  sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 9 \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 9 Terminado...............\n",pthread_self());
   sleep(2); 
  printf("[TH_1:%ld]: Iniciando Proceso 10  \n", pthread_self());
  sleep(5);
  (*(int*)data)++;
  printf("[TH_1: %ld]: Proceso 10 Terminado...............\n",pthread_self());
  pthread_exit(data);
  
}

int main()
{
  pthread_t thread;
  int data=0;
  int thread_rc;
  printf("[MAIN:%ld]: Iniciando Sistema............ \n",pthread_self());
  if ((thread_rc=pthread_create(&thread,NULL,thread_run,&data))!=0)
  {
    printf("Error Creando los Porcesos. Codigo %i");
    return -1;
  }
  sleep(1);
  printf("[MAIN:%ld]: Sistema Iniciado \n",pthread_self());
  int *ptr_output_data;
  pthread_join(thread,(void **)&ptr_output_data);
  printf("[MAIN:%ld]: Procesos Ejecutados %d \n",pthread_self(), *ptr_output_data);
  return 0;
} 

